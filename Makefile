## Makefile downloads from git repository and extracts ttf/otf files. Use
## createdeb.sh to build debian package.

otfsrc=https://github.com/adobe-fonts/source-code-pro/releases/download/2.042R-u%2F1.062R-i%2F1.026R-vf/OTF-source-code-pro-2.042R-u_1.062R-i.zip
otfout=$(shell basename ${otfsrc})

all:
	test -f ${otfout} || wget ${otfsrc}
	unzip ${otfout}

clean:
	rm -f ${otfout}
	rm -rf OTF/
